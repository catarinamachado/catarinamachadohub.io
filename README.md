## Hi there!

This is the repository of my personal website. You can check the page
[here](https://catarinamachado.github.io). Hope you enjoy! :smile:

You can check a repository version of this website in Laravel
[here](https://github.com/catarinamachado/catarinamachado-laravel-version).


## Contributing

The design of this website was made by [HTML5 UP](https://html5up.net/).
Many thanks!
